 .. _connect_eth:

การติดต่อกับ Ethereum
#############################

ด้วยตัวระบบ Ethereum จะมีการเปิดเผย method ผ่าน `JSON-RPC`_ เป็นการตอบโต้จาก Application 
อย่างไรก็ตามการตอบโต้จาก Application ผ่าน JSON-RPC นั่นยังมีสิ่งนักพัฒนาต้องรู้อีกคือ

.. _JSON-RPC: https://github.com/ethereum/wiki/wiki/JSON-RPC

- JSON-RPC protocol
- Binary การ encoding/decoding สำหรับการสร้างและตอบโต้กับ Smart Contract
- 256 bit numeric type
- สำหรับควบคุมสำหรับ Admin เช่นการ create address

จากข้างต้นทำให้การทำ Application ที่ใช้ Blockchain นั้นมีความยากลำบาก จึงมีการเขียน Library สำหรับ Application ที่ต้องการใข้งาน Blockchain 
เพื่อให้ง่ายต่อการพัฒนา การใช้และการตอบโต้กับ Blockchain โดย Library แต่ละอย่างก็มีภาษาเขียนที่รองรับแตกต่างกัน

Web3.js 
*********

Web3.js คือ Library สำหรับติดต่อกับ Blockchain โดยใช้ภาษา Javascript และให้ใช้งานผ่าน npm ที่เป็น node module

.. note::
    สามารถศึกษาข้อมูลเพิ่มเติมเกี่ยวกับ Web3 ได้ที่ `Web3.js version 0.2`_ หรือ `Web3.js version 1.0`_

.. _Web3.js 0.2: https://github.com/ethereum/wiki/wiki/JavaScript-API
.. _Web3.js 1.0: https://web3js.readthedocs.io/en/1.0/