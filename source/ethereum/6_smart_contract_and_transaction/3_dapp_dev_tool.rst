.. _dapp:

DApp
###########

DApp คืออะไร
********************

Dapp ตือ Application ที่เชื่อมต่อกับ Blockchain
โดยทั่วไปแล้ว Ethereum DApps จะเชื่อมต่อผู้ใช้ผ่าน Application โดยใช้ Javascript API 
เพื่อสื่อสารกับ Blockchain โดยทั่วไปแล้ว DApps 
จะมี Smart Contract ที่ถูกเก็บไว้ใน Blockchain แล้วเรียกใช้งาน Smart Contract นั้นๆ

เครื่องมือสำหรับ Developer
##############################
การพัฒนา DApp ต้องมีความเข้าใจเบื้องต้นเกี่ยวกับเครื่องมือ Web3 Javascript-API JSON-RPC และ Solidity 

- `Web3`_ คือ Library สำหรับติดต่อกับ Ethereum Node โดยใช้ภาษา Javascript 
- `JSON-RPC`_ คือ คำสั่งสำหรับใช้ควบคุม Ethereum Node โดยใช้เครื่องมืออื่นๆช่วย เช่น Geth เป็นต้น
- `Solidity`_ คือ ภาษาสำหรับเขียน Smart Contract ใน Ethereum Blockchain
- `Ethlint`_ คือ ตัวช่วยสำหรับเขียน Solidity 

.. _Web3: https://github.com/ethereum/wiki/wiki/JavaScript-API
.. _JSON-RPC: https://github.com/ethereum/wiki/wiki/JSON-RPC
.. _Solidity: https://solidity.readthedocs.io/en/latest/
.. _Ethlint: https://github.com/duaraghav8/Ethlint

Remix Ethereum
**********************
เป็นเครื่องมือสำหรับทดสอบเขียน Smart Contract ในเครื่องจำลอง และสามารถทดสอบ function ใน Smart Contract ได้ด้วย ทำให้ไม่เปลืองทรัพยากร

สามารถใช้งานได้ที่ https://remix.ethereum.org/