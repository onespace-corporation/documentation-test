.. _account_gas_tx:

Account Type
################

Account ใน Ethereum มีอยู่ 2 ประเภทคือ
- Externally Owned Accounts(EOAs)
- Contract Account หรือ Smart Contract Address 

Externally Owned Accounts(EOAs)
************************************

- มี balance 
- สามารถส่ง หรือสร้าง Transaction
- ควบคุมโดย private keys
- ไม่ code เข้ามาเกี่ยวข้อง

Contract Account หรือ Smart Contract Address 
****************************************************************

- มี balance
- มีความเกี่ยวข้องกับ code
- code สามารถดำเนินการได้โดย Transaction หรือ message(call) จาก Smart Contract อื่นๆ
- เมื่อดำเนินการ code จะสามารถเข้าถึง Smart Contract อื่นๆได้เช่นกัน
- มีการจัดการกับ storage และมี state เป็นของตัวเอง

Transaction เปรียบเสมือนการกระทำที่เกิดใน Ethereum Blockchain โดย Externally Owned Accounts ทุกๆครั้งที่ 
Contract Account ได้รับ Transaction จะทำให้ code ที่อยู่ใน Smart Contract ถูกดำเนินการโดยขึ้นอยู่กับพารามิเตอร์ที่ได้รับ 
ซึ่ง code ที่อยู่ใน Smart Contract จะถูกดำเนินการโดย Ethereum Virtual Machine บน node แต่ละตัวที่อยู่ในเครือข่ายเดียวกัน

Transaction
##################

Transaction ใน Ethereum เปรียบเสมือนสัญลักษณ์ที่บ่งบอกถึงข้อมูลต่างๆที่เกิดจาก Externally Owned Accounts ที่กระทำกับ Account อื่นๆใน Blockchain

ใน Transaction ประกอบด้วย

- ผู้รับ message
- signature ของ sender 
- ``VALUE`` คือ จำนวน wei จาก sender
- ส่วนเพิ่มเติมคือ data ซึ่งคือ message ที่ถูกส่งให้ Smart Contract 
- ``GASPRICE`` ค่า fee ที่ sender ให้กับค่า gas

.. note::
    sender คือ Account ที่ใช้ในแนบตอนสร้าง Transaction 

Message 
#################

Smart Contract มีสามารถส่ง message ให้ Smart Contract อื่นได้ ซึ่ง message คือข้อความที่อยู่ใน Ethereum และสามารถเรียกดูได้ด้วย function call

ใน Message ประกอบด้วย

- ผู้ส่งข้อความ
- ผู้รับข้อความ
- ``value`` คือ จำนวน wei จาก sender
- ส่วนเพิ่มเติมคือ data ซึ่งคือ input data จาก Smart Contract

Gas
########

การคำนวณหรือการยืนยัน Transaction ต้องอาศัย Miner ดังนั้นสิ่งที่จะทำให้ Miner มีแรงจูงใจในการคำนวณคือ Gas 

Gas คือค่าธรรมเนียมสำหรับทุก Transaction ที่เกิดขึ้นไม่ว่าจะเป็นการส่งมอบ Ether หรือการคำนวณ Smart Contract หากต้องการทำอย่างใดอย่างหนึ่ง 
จำเป็นต้องเสียค่าธรรมเนียม เพื่อเป็นค่าตอบแทนและแรงจูงใจให้ Miner คำนวณคำสั่ง
