 .. _web3:

ที่มา Web 3: Platform สำหรับ Decentralized Applications
************************************************************

ด้วยเทคโนโลยี Blockchain ที่เปิดกว้าง และมีความน่าเชื่อถือสูงอย่าง Ethereum จึงเหมาะสมนำมาใช้ back end ที่มีการกระจายข้อมูล และองค์กรที่มีการกระจายอำนาจ - Web 3.0

ตามที่นักพัฒนา Ethereum ตั้งใจไว้ Ethereum เป็นผืนผ้าใบที่ว่างเปล่าและทุกคนมีอิสระในการสร้างทุกสิ่งที่คุณต้องการ 
Protocol Ethereum นั้นมีวัตถุประสงค์เพื่อให้เป็น generalized เพื่อให้คุณสมบัติที่หลากหลายสามารถรวมกันได้ 
จึงเกิดเป็น DApp ที่ใช้งานบน Ethereum Blockchain เพื่อสร้าง Solution ต่างๆตามที่ผู้พัฒนาต้องการ

ที่มา Smart Contact
======================
by Alex:

Would you enter in a Smart Contract with someone you’ve never met? Would you agree to lend money to some farmer in Ethiopia? Would you become an investor in a minority-run newspaper in a war zone? Would you go to the hassle of writing up a legal binding Smart Contract for a $5 dollar purchase over the internet?

The answer is no for most of these questions, the reason being that a Smart Contract requires a large infrastructure: sometimes you need a working trust relationship between the two parties, sometimes you rely on a working legal system, police force and lawyer costs.

In Ethereum you don’t need any of that: if all the requisites to the Smart Contract can be put in the blockchain then they will, in a trustless environment for almost no cost.

Instead of thinking of moving your current Smart Contracts to the blockchain, think of all the thousand little Smart Contracts that you would never agree to simply because they weren’t economically feasible or there was not enough legal protection..

