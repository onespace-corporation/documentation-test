 .. _account_management:

จัดการ Account
################

Accounts
**********

Accounts คือตำแหน่งหนึ่งที่อยู่ใน Ethereum โดย Account จะมีอยู่ 2 ประเภทคือ

- Externally Owned Accounts(EOAs) จะถูกควบคุมโดย Private Keys
- Contract Accounts ถูกควบคุมโดย Smart Contract Code และ Activated โดย EOA เท่านั้น

Contract Account จะพูดถึงในบทความถัดๆไปในบทความนี้จะพูดถึง Externally Owned Accounts(EOAs) ซึ่งแนวคิดที่มี Account นั้น
เปรียบเสมือนเป็นเครื่องหมายยืนยันตัว บ่งบอกถึงสถานะปัจจบัน หรือเรียกว่า state object โดย state ของ Account สามารถพูดได้หลายอย่าง เช่น
Account มี balance และ Smart Contract มีทั้ง balance และ Smart Contract storage

state ของทุกๆ Account เปรียบก็คือ state ของ Ethereum Network นั้นๆ ซึ่งจะถูก Update ทุกๆ Block และ Network นั้นๆจะใช้ระบบ Consensus
(พูดง่ายๆก็คือ ทุกๆคนที่อยู่ใน Network เดียวกัน เห็นพ้องต้องกันว่า state ที่ Update ถูกต้อง) ดังนั้น Account เป็นสิ่งที่จำเป็นสำหรับผู้ใช้งานใน Ethereum blockchain ในการเกิด Transaction

แต่ถ้าระบบ Ethereum มีแต่ Externally Owned Accounts(EOAs) และอนุญาติให้เกิดแค่ Transaction ระหว่าง Account ระบบนี้จะถูกเรียกว่า 
ระบบ altcoin ซึ่งมีประสิทธิภาพต่ำกว่า bitcoin และทำได้แค่ส่ง ether

Account ยังใช้ public key เพื่อลงนามในการทำธุรกรรม เพื่อให้ EVM สามารถตรวจสอบความถูกต้อง
ของ Account ของผู้ทำธุรกรรมและเป็นการยืนยันตัวตนของ Account ด้วย

Keyfile
***********

ทุกๆ Account ถูกกำหนดด้วย private key และ public key และ Account ยังถูกกำหนดไว้ในรูปแบบ address ซึ่งได้มาจาก 20 bytes ล่าสุดของ public key 
ทุกๆ private key กับ address จะถูก encode ลงใน keyfile ซึ่ง keyfile จะอยู่ในรูปแบบไฟล์ JSON text สามารถดูไฟล์และแก้ไขไฟล์โดย text editor

ส่วนที่สำคัญของ keyfile คือ keyfile นี้จะถูก encrypt ด้วย password ที่ตั้งไว้ตอนสร้าง Account และ keyfile สามารถหาได้ใน ``keystore`` 
ซึ่งอยู่ใน directory ย่อยที่อยู่ใน directory ของ Ethereum Node

อย่างไรก็ตามการสร้าง Account ขึ้นมาใหม่นั่นหมายความคุณไม่มี Ether อยู่ในบัญชีของคุณ แต่อย่างน้อย Account นี้ก็เป็นของคุณเพราะการเข้าถึง Account นี้ต้องใช้ 
password และ key ซึ่งมีแต่คุณเท่านั้นที่รู้

