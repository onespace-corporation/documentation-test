.. _whatisworkspace:

Workspace คืออะไร?
##########################

Workspace คือ เครือข่ายของ Blockchain และทุกๆ Node ที่อยู่ใน Workspace เดียวกันจะมีการ Connect กันอยู่ตลอดเวลา ทำให้ข้อมูลทุกอย่างจะมีเท่ากัน

.. note::
    1 Network = 1 Workspace

Node คืออะไร?
####################

Node คือ Computer ที่เก็บข้อมูล  Blockchain ต่างๆไว้ เปรียบเสมือน Cloud ของ Blockchain ซึ่งจะประกอบไปด้วย Block หลายๆ Block ต่อกันอยู่

.. note::
    1 Network สามารถมีได้หลาย Node

