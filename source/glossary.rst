.. _glossary:

Glossary
##############

Address
    ตัวตนที่ใช้ยืนยันใน Ethereum Blockchain คือ Account ซึ่งเป็นชนิด Address เป็นข้อมูลขนาด 20 Bytes และเก็บในรูปแบบฐาน 16 
    สามารถเรียกใช้งานด้วยการเติม ``0x`` ไว้ด้านหน้า เช่น ``0xcd2a3d9f938e13cd947ec05abc7fe734df8dd826``

Block
    กล่องที่เกิดขึ้นใน Blockchain ซึ่งในกล่องจะข้อมูลต่างๆไว้ เช่น Transaction Number เป็นต้น ซึ่งกล่องทุกๆจะมีการต่อกันเป็นทอดๆ อ้างอิงจากค่า Hash

bootnode
    node ที่มีการบันทึกข้อมูลของทุกๆ node หรือ node ที่มี peer กับทุกๆ node ที่อยู่ใน network เดียวกัน

Coinbase
    Account ที่ได้รับผลตอบแทนการ Mining ใน Node นั้นๆ

Complier
    โปรแกรมแปลงภาษาระดับสูงให้กลายเป็นภาษาระดับต่ำ

DAG
    Directed Acyclic Graph มันคือกราฟชุดของ Node ซึ่งเป็นคุณสมบัติการต่อกันระหว่าง Node 
    Ethereum ใช้ DAG ใน Ethash ซึ่งเป็นอัลกอริทึมของ Ethereum Proof of Work (POW) 
    Ethash DAG ใช้เวลานานในการสร้างซึ่งทำโดย Miner Node เป็นไฟล์แคชแต่ละ Epoch
    ข้อมูลไฟล์จะถูกใช้เมื่ออัลกอริทึมต้องการค่าจากกราฟนี้

Decentralized Application
    Service ที่ไม่มีการเชื่อใจบุคคลที่สามหรือไม่ผ่านคนกลางนั่นเอง แต่จะเชื่อใจตัวระบบหรือ resource แทน

EOA 
    Externally Owned Account คือ Account ที่แสดงตัวตรใน Ethereum Blockchain โดยสามารถเข้าใช้งานด้วย passphase หรือ private key 

Epoch
    เป็นการแบ่ง Block ของ Ethereum Blockchain ซึ่งจะมีการ GEN DAG ทุกๆ 1 Epoch หรือทุกๆ 30,000 Block 

Ether 
    สกุลเงินหลักใน Ethereum Blockchain ซึ่งใช้ในการดำเนินบัญชีต่างๆใน Ethereum Virtual Machine 

EVM
    Ethereum Virtual Machine คือคอมพิวเตอร์ที่ไม่มีศูนย์กลางซึ่งทำงานด้วย Ethereum Platform

Gas 
    ค่าธรรมเนียมสำหรับการทำธุรกรรมใน Ethereum Blockchain หรือเรียกว่าหน่วยของการค่าทำธุรกรรมก็ได้

genesis block 
    Block แรกของ network นั้นๆ

Hash
    Hash คือนำการค่าข้อมูลบางอย่างนำไปในฟังก์ชันรหัสลับแล้วมีค่าตอบกลับมาเป็นชุดตัวเลข และ string ขนาดคงที่ หรือเรียกว่า SHA3 ซึ่งใน Ethereum Blockchain 
    Hash ที่ได้เกิดจากข้อมูลต่างๆภายใน Block เช่น previousHash Input Data Date เป็นต้น ทั้งนี้ทำให้ข้อมูลต่างๆภายใน Block มีความสำคัญ เพราะจะส่งผลกระทบกับค่า Hash ด้วย

log event
    ใน Ethereum Blockchain การทำธุรกรรมสักอย่างหนึ่งต้องมี Transaction เกิดขึ้น ซึ่งปกติการเกิด Transaction หนึ่งก็จะข้อมูลเพื่อบ่งบอกถึงการทำธุรกรรมนั้นแน่นอน
    โดยปกติหากต้องการดูข้อมูลที่อยู่ใน Smart Contract ต้อง Call เพื่อดูข้อมูลตามที่ Smart Contract นั้นต้องการแสดงออกมาได้ แต่ log event สามารถทำให้ข้อมูลนั้นแนบมาพร้อมกับ 
    Transaction ได้ ดังนั้น log event เปรียบเสมือนการนำข้อมูลมาเก็บไว้ใน Transaction 

Mining
    การขุดใน Ethereum Blockchain เพื่อเป็นการยืนยันข้อมูลที่เกิดขึ้นใน Ethereum Blockchain และดำเนินการ Smart Contract ด้วย ซึ่งผลตอบแทนที่ได้จากการขุดก็คือ Ether

nonce
    ค่าๆหนึ่งที่เปลี่ยนแปลงไปตามกาลเวลาและค่าปัจจัยอื่นๆ สร้างขึ้นมาเพื่อตรวจสอบ session ซึ่งใน Blockchain ใน Protocal Proof of Work ค่า nonce คือค่า 
    Hash เป็นสิ่งที่ Mining ต้องหาเพื่อตรวจสอบความถูกต้องของ Block

Passphase
    เปรียบเสมือน password ถาวรที่ใช้เข้าถึง Account นั้นๆ ซึ่งไม่สามารถแก้ไขได้

Peer
    การที่ Node เชื่อมต่อกัน ทำให้มีข้อมูลเหมือนกัน ซึ่งการ Peer ครั้งแรก Node ที่มี Block ต่ำกว่าจะต้องยึดตาม Node ที่มี Block สูงสุด สิ่งนี้เรียกว่า GEN DAG

Private key
    เปรียบเสมือนกุญแจถาวรที่ใช้เข้าถึง Account และผู้ที่ถือกุญแจเท่านั้นที่สามารถเข้าถึง Account นั้นได้

Smart Contract
    ธุรกรรมแบบถาวรที่ถูกเก็บไว้ใน Ethereum Blockchain จะสามารถทำงานได้ก็เมื่อมี Input เข้ามา หลังจากการจะมีผลตอบกลับเป็นอย่างไรขึ้นอยู่กับ Smart Contract นั้นๆ

Solidity
    ภาษาหลักที่ใช่ในการเขียน Smart Contract 

State 
    บ่งบอกถึงการเคลื่อนไหวของ Account และข้อมูลต่างๆใน Ethereum Blockchain เช่น Smart Contract เป็นต้น ซึ่ง State จะแปรผันตาม Block ที่เกิดขึ้นล่าสุด

Transaction
    รายการเดินบัญชีที่เกิดขึ้นใน Blockchain ซึ่งทุกการกระทำที่มีการบันทึกข้อมูลหรือมีการเปลี่ยนแปลงข้อมูลจะถูกเก็บไว้ใน Transaction เช่น Input Data 
    Value เป็นต้น ซึ่ง Transaction แต่ละ Transaction อาจมีฟอร์มข้อมูลไม่เหมือนกัน เนื่องจากการกระทำใน Blockchain นั้นมีหลากหลายรูปแบบ

trustless
    เครือข่ายที่ไม่ต้องใช้ความน่าเชื่อใจของบุคคลใดบุคคลหนึ่งแต่เชื่อใจในตัวระบบแทน

Web3
    Library สำหรับติดต่อกับ Ethereum Blockchain โดยใช้ภาษา Javascript
