.. onespace_platform documentation master file, created by
    sphinx-quickstart on Thu Jan  3 12:10:05 2019.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to Onespace Platform's documentation!
**********************************************

.. image:: images/logo3.png
    :target: https://www.onespace.co.th/
    :alt: logo
    :align: center


**OneSpace** คือ ผู้ให้บริการ Infrastructure สำหรับใช้งาน Blockchain Technology (OneSpace Platform) ที่มาพร้อมกับเครื่องมือบน Ethereum Platform ช่วยให้การขึ้นระบบทำได้รวดเร็วยิ่งขึ้น 
ให้คำปรึกษาเรื่องการนำ Blockchain เข้าไปช่วยเพิ่มประสิทธิภาพในองค์กรของท่าน ออกแบบและพัฒนาระบบ รวมไปถึงการให้บริการหลังการขาย

ให้คำปรึกษาเรื่องการนำ Blockchain เข้าไปช่วยเพิ่มประสิทธิภาพในองค์กรของท่าน อย่างครบวงจร ตั้งแต่การศึกษาปัญหา ออกแบบและพัฒนาระบบ 
รวมไปถึงการให้บริการหลังการขาย

สามารถใช้บริการของเราได้ที่ `OneSpace Platform`_

สำหรับผู้ที่ต้องการเริ่มต้นใช้งาน OneSpace Platform แนะนำให้เริ่มจาก `Quick Start`_

.. _OneSpace Platform: https://www.onespace.co.th/
.. _Quick Start: quickstart.html

.. toctree::
    :maxdepth: 2
    :caption: สารบัญ:

    0_introduction.rst
    1_quickstart.rst
    2_tutorial.rst
    3_ethereum.rst
    price_rate.rst
    qanda.rst
    glossary.rst
    contact.rst
