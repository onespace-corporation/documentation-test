.. _ethereum:

Ethereum
##############

.. image:: images/ethereum_logo.png
    :alt: logo_eth
    :width: 200pt
    :align: center

Ethereum เป็นหนึ่งใน Protocol ของ OneSpace Platform ซึ่ง Document นี้จัดทำเพื่อช่วยเหลือ User ให้เข้าใจเกี่ยวกับ Ethereum มากขึ้น โดยข้อมูลส่วนใหญ่นำมาจาก `Ethereum Document`_ 

.. _Ethereum Document: http://www.ethdocs.org/en/latest/index.html

.. toctree::
    :maxdepth: 2
    :caption: สารบัญ:

    ethereum/1_introduction.rst
    ethereum/2_connect_eth.rst
    ethereum/3_account_management.rst
    ethereum/4_ether.rst
    ethereum/5_mining.rst
    ethereum/6_smart_contract_and_transaction.rst

 